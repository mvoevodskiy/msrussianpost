/**
 * Created by mvoevodskiy on 28.10.15.
 */
$(function(){

    // события на контролы
    $('#city_select').on('change', function(){
        miniShop2.Order.add('city', $(this).val());
    });
    $('#country_select').on('change', function(){
        miniShop2.Order.add('country', $(this).val());
    });

    miniShop2.Callbacks.add('Order.add.response.success', 'msRussianPostOrderAdd', function(response) {
        //console.log(response);
        //if (response.data != undefined && response.data.country != undefined) {
        //    $('#country_select').val(response.data.country);
        //    $.post('/assets/components/msrussianpost/action.php', {msdelivery_action: "delivery/getcities"}, function (success) {
        //        miniShop2.Order.add('city', '');
        //        $('#city_select').html('<option value="">  Выберите  </option>' + success);
        //    });
        //}

        if (response.data != undefined && (response.data.city != undefined || response.data.delivery != undefined)) {
            miniShop2.Order.getcost();
        }
    });

    miniShop2.Callbacks.add('Order.getcost.response.success', 'msRussianPostOrderGetсost', function(response) {
        // получаем срок
        // из-за него кстати сказать и происходила перезагрузка страницы, вернее из-за того что плагин возвращал код страницы если был выбран другой способ доставки
        $.post('/assets/components/msrussianpost/action.php',{msdelivery_action: "delivery/gettime"}, function(data){
            var deliverytimespan = $('#ms2_delivery_notify');
            if (deliverytimespan.length>0) {
                button = $('#msOrder button[type="submit"]');
                deliverytimespan.html(data.message);
                if (data.success)  {
                    button.removeAttr('disabled');
                } else {
                    button.attr('disabled','disabled');
                }
            }
        }, 'json');

        //if (document.getElementById('deliveryPoints') !== null) {
        //    $.post('/assets/components/msrussianpost/action.php', {
        //        msdelivery_action: "delivery/getPoints",
        //        city: $('#city_select').val()
        //    }, function (data) {
        //        dpSelect = document.getElementById('deliveryPoints');
        //        if (data.points.length > 0) {
        //            dpSelect.parentNode.parentNode.style.display = '';
        //        } else {
        //            dpSelect.parentNode.parentNode.style.display = 'none';
        //        }
        //        dpSelect.innerHTML = '<option value="">  Выберите пункт выдачи  </option>';
        //        data.points.forEach(function (value) {
        //            opt = '<option value="' + value.address + '">' + value.address + '</option>';
        //            dpSelect.innerHTML = dpSelect.innerHTML + opt;
        //        });
        //    }, 'json');
        //}
    });
});